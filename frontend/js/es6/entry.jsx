import React from 'react';
import ReactDOM from 'react-dom';
import {RouteOptimization} from './source/route-optimization';
import {ReactUtils} from './source/react-utils';
import mapboxgl from 'mapbox-gl';

import '../../sass/main.sass';
import config from '../../creds/mapbox.json';

class FlyingCrowApp extends React.Component{
    constructor(props){
        super(props);

        this.defaultPos = [-0.1048817, 51.5238446];
        this.eClickModes = Object.freeze({
            "disabled": 0,
            "hqCreate": 1,
            "destCreate": 2
        });

        this.homeMarker = null;
        this.destMarkers = [];
        this.bRouteLineDrawn = false;

        this.state = {
            clickMode: this.eClickModes.disabled,
            resultsOverlay: false,
            errorMsg: null,
            resultsData: {}
        }
    }

    // Initialization

    componentDidMount(){
        mapboxgl.accessToken = config.apiKey;
        this.glMap = new mapboxgl.Map({
            container: this.mapContainer,
            style: 'mapbox://styles/antonyaustin/cjn0v8gca55d52ss97a5bmcca',
            center: this.defaultPos,
            zoom: 5,
            bearing: 90
        });

        this.initMap();

        window.combineTest = RouteOptimization.combineDestinations;
    }

    componentWillUnmount(){
        if(this.glMap)
            this.glMap.remove();
    }

    initMap(){
        // Fly to home pos at 14 zoom
        this.glMap.flyTo({
            zoom: 14,
            bearing: 0
        })

        this.glMap.on('click', this.mapClicked.bind(this));
    }

    // Utility

    createMarkerEl(classString){
        let el = document.createElement('div');

        el.className = 'fcs-marker ' + classString;

        return el;
    }

    createRouteLine(points, order){
        let layer = {
            "id": "salesRoute",
            "type": "line",
            "source": {
                "type": "geojson",
                "data": {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": "LineString",
                        "coordinates": []
                    }
                }
            },
            "layout": {
                "line-join": "round",
                "line-cap": "round"
            },
            "paint": {
                "line-color": "#ff8400",
                "line-width": 4,
                'line-dasharray': [3, 2],
            }
        }

        let bounds = new mapboxgl.LngLatBounds()

        order.map(item => {
            let thisPoint = points[item],
                coords = thisPoint.getLngLat(),
                coordArr = [coords.lng, coords.lat];
                
            layer.source.data.geometry.coordinates.push(coordArr);
            bounds.extend(coordArr);
        })

        this.glMap.addLayer(layer);
        this.glMap.fitBounds(bounds, {
            padding: 50
        });
        this.bRouteLineDrawn = true;
    }

    removeRouteLine(){
        if(!this.glMap.getLayer("salesRoute"))
            return;

        this.glMap.removeLayer("salesRoute");
        this.glMap.removeSource("salesRoute");
        this.bRouteLineDrawn = false;
    }

    displayError(message){
        this.setState({
            errorMsg: message
        })
    }

    clearError(){
        this.setState({
            errorMsg: null
        })
    }

    // User Interaction

    setClickMode(mode){
        this.clearError();

        this.setState({
            clickMode: mode
        })
    }

    mapClicked(e){
        if(this.state.clickMode == this.eClickModes.hqCreate)
            this.createHomeMarker(e.lngLat);

        else if(this.state.clickMode == this.eClickModes.destCreate)
            this.createDestMarker(e.lngLat);
    }

    createMarker(pos, className){
        return new mapboxgl.Marker(this.createMarkerEl(className))
            .setLngLat(pos)
            .addTo(this.glMap);
    }

    createHomeMarker(pos){
        if(this.homeMarker != null)
            this.homeMarker.remove();

        this.homeMarker = this.createMarker(pos, 'fcs-marker-home');

        setTimeout((() => {
            this.setState({
                clickMode: this.eClickModes.disabled
            })
        }).bind(this), 400)
        
    }

    createDestMarker(pos){
        if(this.destMarkers.length < 8)
            this.destMarkers.push(this.createMarker(pos, 'fcs-marker-dest'));

        if(this.destMarkers.length >= 8)
            setTimeout((() => {
                this.setState({
                    clickMode: this.eClickModes.disabled
                })
            }).bind(this), 400)
    }

    computeRoute(){

        if(!this.homeMarker){
            this.displayError("Please specify a home position by clicking the nest icon below.");
            return;
        }

        if(this.destMarkers.length < 1){
            this.displayError("Please add at least 1 sales destination to your route.");
            return;
        }

        if(this.bRouteLineDrawn)
            this.removeRouteLine();

        let result = RouteOptimization.shortestPath(this.homeMarker, this.destMarkers.slice()),
            distKm = result.dist * 1.60934,
            lpdKm = result.longestPointDistance * 1.60934,
            feathers = Math.floor(result.dist * 4);

        this.createRouteLine(result.points, result.combo);

        this.setState({
            resultsOverlay: true,
            resultsData: {
                dist: result.dist.toFixed(2),
                distKm: distKm.toFixed(2),
                calcTime: result.calcTime.toFixed(2),
                pointsCount: this.destMarkers.length,
                longestPointDistance: result.longestPointDistance.toFixed(2),
                lpdKm: lpdKm.toFixed(2),
                feathers: feathers
            }
        })
    }

    resetMarkers(){
        if(this.homeMarker != null){
            this.homeMarker.remove();
            this.homeMarker = null;
        }

        if(this.destMarkers.length){
            this.destMarkers.map((item, index) => {
                item.remove();
            })
            this.destMarkers = [];
        }

        this.removeRouteLine();
    }

    returnToMenu(){
        this.setState({
            clickMode: this.eClickModes.disabled,
            resultsOverlay: false
        })
    }

    // Classes

    getToolbarClass(){
        return ReactUtils.prepareClass({
            "fcs-toolbar-overlay": true,
            "fcs-overlay-element": true,
            "fcs-active": this.state.clickMode != this.eClickModes.disabled
        })
    }

    getResultsOverlayClass(){
        return ReactUtils.prepareClass({
            "fcs-results-overlay": true,
            "fcs-overlay-element": true,
            "fcs-active": this.state.resultsOverlay == true
        })
    }

    // Rendering

    renderError(){
        if(!this.state.errorMsg)
            return null;

        return(
            <div className="fcs-error">{this.state.errorMsg}</div>
        );
    }

    renderToolbar(){
        return(
            <div className={this.getToolbarClass()}>
                <div onClick={e => this.returnToMenu()} className="fcs-ui-button fcs-back fcs-halfsize"></div>
            </div>
        )
    }

    renderResultsOverlay(){
        return(
            <div className={this.getResultsOverlayClass()}>
                <div className="fcs-results-header">
                    <h2>
                        <div onClick={e => this.returnToMenu()} className="fcs-ui-button fcs-back fcs-halfsize"></div>
                        Results
                    </h2>
                </div>
                <p>
                    Optimal route for {this.state.resultsData.pointsCount} sales destinations calculated in {this.state.resultsData.calcTime}ms.
                </p>
                <ul>
                    <li><strong>Total Distance:</strong> {this.state.resultsData.dist}mi ({this.state.resultsData.distKm}km)</li>
                    <li><strong>Longest Point Distance:</strong> {this.state.resultsData.longestPointDistance}mi ({this.state.resultsData.lpdKm}km)</li>
                    <li><strong>Feathers Dropped:</strong> {this.state.resultsData.feathers}</li>
                </ul>
            </div>
        )
    }

    renderOverlay(){
        if(this.state.clickMode != this.eClickModes.disabled || this.state.resultsOverlay == true)
            return null;

        return(
            <div className="fcs-overlay-outer">
                <div className="fcs-overlay-inner fcs-overlay-element">
                    <h1>The Flying Crow Salesman's Assistant</h1>
                    {this.renderError()}
                    <p>
                        <strong>Quoth the Raven:</strong> Trust the Flying Crow Salesman's Assistant to find your perfect sales route for maximum efficiency. Simply enter your office location and up to eight destinations and we'll plan your perfect route!
                    </p>

                    <div>
                        <div className="fcs-ui-button-container">
                            <div className="fcs-ui-button fcs-home" onClick={e => {this.setClickMode(this.eClickModes.hqCreate)}}>
                                <div className="fcs-btn-label">Add Home Location</div>
                            </div>
                            <div className="fcs-ui-button fcs-dest" onClick={e => {this.setClickMode(this.eClickModes.destCreate)}}>
                                <div className="fcs-btn-label">Add Destinations</div>
                            </div>
                            <div className="fcs-ui-button fcs-compute" onClick={e => {this.computeRoute()}}>
                                <div className="fcs-btn-label">Compute Route</div>
                            </div>
                            <div className="fcs-ui-button fcs-reset" onClick={e => {this.resetMarkers()}}>
                                <div className="fcs-btn-label">Reset Points</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }

    render(){
        return(
            <div className="fcs-inner">
                <div className="fcs-map" ref={el => this.mapContainer = el}></div>
                {this.renderToolbar()}
                {this.renderResultsOverlay()}
                {this.renderOverlay()}
            </div>
        );
    }
}

ReactDOM.render(
    <FlyingCrowApp />
, document.getElementById('fcs-inject'));