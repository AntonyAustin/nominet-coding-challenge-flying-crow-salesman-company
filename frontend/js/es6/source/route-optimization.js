export class RouteOptimization{
    static combineDestinations(arr, index = -1, combinations = []){
        //Recursive function to get an array of possible destination visit orders.

        let allCombos = [];

        if(arr.length < 2){
            console.error('Array too short!');
            return;
        }

        // If index is at the default -1, seed the recursive process with a simple, two-element, two-combination array.
        if(index == -1){
            let startArray = [arr[0], arr[1]];
            allCombos.push(startArray.slice());
            allCombos.push(startArray.slice().reverse());
            index = 2;

            // return here if it's only a 2-element array, otherwise proceed to the next recursion step
            if(arr.length == 2)
                return allCombos;
            else
                return this.combineDestinations(arr, index, allCombos);
        }

        // We can then work out the combinations with an extra element in the mix, which we'll call indexEl.
        // A new indexEl will get added to the results each time, until we reach the end of arr.
        let indexEl = arr[index];

        for(let i=0; i<combinations.length; i++){
            let thisCombination = combinations[i].slice(),
                baseCombo = thisCombination;

            baseCombo.unshift(indexEl);

            // Push the first possible combination, which is the additional index unshifted onto the beginning of the array.
            allCombos.push(baseCombo);

            for(let j=0; j<thisCombination.length - 1; j++){
                let thisCombo = baseCombo.slice();

                // Insert the new element into all possible gaps in each combination by swapping it with each existing element.
                thisCombo[0] = thisCombo[j+1];
                thisCombo[j+1] = indexEl;

                allCombos.push(thisCombo);
            }
        }
        index++

        if(index >= arr.length)
            return allCombos;
        else
            return this.combineDestinations(arr, index, allCombos);
    }

    static shortestPath(home, points){

        /*
        * home: the home point, to be added to the start and end of the other points.
        * points: an array of all the destination points.
        */

        let t0 = performance.now(),
            indexArr = [],
            min = Infinity, // running minimum distance
            minCombo = null,
            distMemo = {}, // Stores precalculated point distances for efficiency
            longestPointDistance = 0;

        points.map((point, index) => {
            // index + 1 as 0 is reserved to refer to the home position
            indexArr.push(index + 1);
        })

        points.unshift(home);

        // Get possible combinations from recursive brute-force function
        let combinations = this.combineDestinations(indexArr);

        console.log(indexArr);

        for(let i=0; i<combinations.length; i++){
            let prevPoint = null,
                prevIndex = null,
                thisCombo = combinations[i],
                currentComboDist = 0,
                currentComboLpd = 0;

            // Cap each combo array on both ends with the home position (0)
            thisCombo.unshift(0);
            thisCombo.push(0);

            for(let j=0; j<thisCombo.length; j++){

                let thisIndex = thisCombo[j],
                    thisPoint = points[thisIndex];

                if(!thisPoint){
                    console.error("Point mismatch");
                    return;
                }

                // prevPoint isn't defined so we're looking at the first point in the combo. Store and progress to the next one.
                if(!prevPoint){
                    prevPoint = thisPoint;
                    prevIndex = thisIndex;
                    continue;
                }

                // memoize the point distances by means of a hash governed by the lower and higher point index numbers
                let lowIndex = Math.min(prevIndex, thisIndex),
                    highIndex = Math.max(prevIndex, thisIndex),
                    pointDist = distMemo[`${lowIndex}-${highIndex}`];

                if(pointDist > currentComboLpd)
                    currentComboLpd = pointDist;

                // Work out the distance if it is not already stored
                if(!pointDist){
                    pointDist = this.getDistance(prevPoint.getLngLat(), thisPoint.getLngLat());
                    distMemo[`${lowIndex}-${highIndex}`] = pointDist;
                }

                // Add the distance to the running total for this combination
                currentComboDist += pointDist;

                // break out of the current loop if we're already over the minimum
                if(currentComboDist > min)
                    break;

                prevPoint = thisPoint;
                prevIndex = thisIndex;
            }

            if(currentComboDist <= min){
                min = currentComboDist;
                minCombo = thisCombo;
                longestPointDistance = currentComboLpd;
            }

        }

        // Get the run time of the algorithm
        let t1 = performance.now();

        return {
            "dist": min,
            "combo": minCombo,
            "points": points,
            "memo": distMemo,
            "calcTime": t1-t0,
            "longestPointDistance": longestPointDistance
        }
    }

    static toRadians(degs){
        return degs * (Math.PI / 180);
    }

    static getDistance(point1, point2){
        // Use the Haversine Formula to get the distance between two points on a sphere. 
        // A big one, with water and continents.

        let earthMeanRadius = 3959, // in miles
            lat1Rad = this.toRadians(point1.lat),
            lat2Rad = this.toRadians(point2.lat),
            deltaLatRad = this.toRadians(point2.lat - point1.lat),
            deltaLonRad = this.toRadians(point2.lng - point1.lng),
            a = Math.sin(deltaLatRad / 2) * Math.sin(deltaLatRad / 2) +
                Math.cos(lat1Rad) * Math.cos(lat2Rad) *
                Math.sin(deltaLonRad / 2) * Math.sin(deltaLonRad / 2),
            c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)),
            d = earthMeanRadius * c;

        return d;
    }
}