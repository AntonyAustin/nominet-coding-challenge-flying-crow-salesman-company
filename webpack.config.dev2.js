path = require('path');

module.exports = {
    entry: {
        mapbox: ['mapbox-gl'],
        react: ['react', 'react-dom'],
        main: ['babel-polyfill', './frontend/js/es6/entry.jsx']
    },
    mode: "production",
    output: {
        path: path.join(__dirname, 'build'),
        filename: '[name].chunkhash.bundle.js',
        chunkFilename: '[name].chunkhash.bundle.js',
        publicPath: '/'
    },
    module: {
        rules: [
            {
                test: /.jsx?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env', '@babel/preset-react']
                    }
                }
            },
            {
                test: /.sass?$/,
                include: /sass/,
                exclude: /node_modules/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /.jpg|gif|png$/,
                exclude: /node_modules/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '/static/images/[name].[ext]'
                    }
                }
            }
        ]
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                react: {
                    chunks: 'initial',
                    name: 'react',
                    test: 'react',
                    enforce: true
                },
                mapbox: {
                    chunks: 'initial',
                    name: 'mapbox',
                    test: 'mapbox',
                    enforce: true
                }
            }
        },
        runtimeChunk: true
    }
}