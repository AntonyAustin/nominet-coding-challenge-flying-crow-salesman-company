# The Flying Crow Sales Company
#### Nominet Coding Challenge

_written by Antony Austin_

A route optimization app designed to show frontend capability both using UI libraries like React and in vanilla Javascript.

The app runs on a simple Node.js server running Express and the Pug templating engine, and the frontend script files are written in next-gen Javascript (ES6) and React JSX. This is all transpiled and compressed using the Babel loader for Webpack 4. Webpack is also equipped with a SASS loader for the styles. For the purposes for this exercise, only one frontend bundle is generated at present.

To run, install dependencies using `npm i`, build frontend files using `npm run build`, then start the server using either `node entry-server.js` or `npm run start`. The page should be visible at [localhost:3000](localhost:3000).