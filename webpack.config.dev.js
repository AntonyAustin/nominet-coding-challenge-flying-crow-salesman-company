module.exports = {
    entry: ['babel-polyfill', './frontend/js/es6/entry.jsx'],
    mode: "development",
    output: {
        path: `${__dirname}/build`,
        filename: 'bundle.js'
    },
    watch: true,
    devtool: 'cheap-module-eval-source-map',
    module: {
        rules: [
            {
                test: /.jsx?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env', '@babel/preset-react']
                    }
                }
            },
            {
                test: /.sass?$/,
                include: /sass/,
                exclude: /node_modules/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /.jpg|gif|png$/,
                exclude: /node_modules/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '/static/images/[name].[ext]'
                    }
                }
            }
        ]
    }
}