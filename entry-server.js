var express = require('express'),
    pug = require('pug'),
    app = express();
    
app.set('view engine', 'pug');
app.use('/build', express.static('build'));
app.use('/static', express.static('static'));

app.get('/', (req, res) => {
    res.render('index', {title: 'Hello', message: 'Test'});
})

console.log("Flying Crow Node server listening on port 3000. The page should be visible on localhost:3000");
app.listen('3000');